#include <stdio.h>
#include "mathtest/math.h"

int main()
{
    printf("\n");

    int num1 = 0;
    int num2 = 0;

    printf("Nova implementace kalkulacky.\n");
    printf("Zadejte prvni cislo:\n");
    scanf("%d", &num1);
    printf("Zadejte druhe cislo:\n");
    scanf("%d", &num2);
    printf("\n");


    printf("Scitani: %d\n", Scitani(num1, num2));
    printf("Odecitani: %d\n", Odecitani(num1, num2));
    printf("Nasobeni: %d\n", Nasobeni(num1, num2));
    
    int vysledekDeleni = Deleni(num1, num2);

    if (vysledekDeleni < 0)
        printf("Nulou delit nelze!\n");
    else
        printf("Deleni: %d\n", Deleni(num1, num2));

    printf("\n\n");

    return 0;
}